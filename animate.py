#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 16:42:19 2017

@author: magdadubois
"""

import matplotlib.pyplot as plt
import matplotlib.animation
try:
    from IPython.display import display
    from IPython.display import HTML
    has_display = True
except ImportError:
    has_display = False  # Skip if IPython not installed

import numpy as np
from parameters import *

#######################
##### Visualizing #####
#######################

def animate(history):
    frames = len(history)
    print("Rendering %d frames..." % frames)
    fig = plt.figure(figsize=(6, 2))
    fig_grid = fig.add_subplot(121)
    fig_health = fig.add_subplot(253)
    fig_visible = fig.add_subplot(254)
    fig_visible_scaled = fig.add_subplot(2,5,10)
    fig_visible_larger = fig.add_subplot(255)
    fig_visible_previous_pos = fig.add_subplot(259)
    fig_health.set_autoscale_on(False)
    health_plot = np.zeros((frames, 1)) 

    def render_frame(i):
        if i%((num_timesteps+1)/4) == 0 :
            print("frame (25%) :", i)
        grid, visible, visible_scaled, visible_larger, reward, visible_previous_pos = history[i]
        # Render full grid
        fig_grid.matshow(grid, vmin=-1, vmax=1, cmap='jet')
        # Render visible grid
        fig_visible.matshow(visible, vmin=-1, vmax=1, cmap='jet')
        fig_visible_scaled.matshow(visible_scaled, vmin=-1, vmax=1, cmap='jet')
        fig_visible_larger.matshow(visible_larger, vmin=-1, vmax=1, cmap='jet')
        fig_visible_previous_pos.matshow(visible_previous_pos, vmin=-1, vmax=1, cmap='jet')
        # Render reward chart
        health_plot[i] = reward
        fig_health.clear()
        #fig_health.axis([0, frames, 0, 10])
        fig_health.axis([0, frames, -10, 10]) #CHANGE
        fig_health.plot(health_plot[:i + 1])

    anim = matplotlib.animation.FuncAnimation(
        fig, render_frame, frames=frames, interval=100
    )
    
    print("animateeeeeeeeeee")
    anim.save('continuous_interpolation.mp4')
    plt.close()
    if has_display:
        display(HTML(anim.to_html5_video()))

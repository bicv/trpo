#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 16:44:36 2017

@author: magdadubois
"""
#######################
##### PARAMETERS ######
#######################

num_episodes = 5
num_timesteps = 5
batch_n = 7

GRID_SIZE = 14

gif_frequency = num_episodes
eps = 1e-15
MAX_KL = 1e-2
DAMPING = 1e-1
d = 1000000 #rounding decimal in TESTING
DOWNSCALE_RATIO = 2
VISIBLE_RADIUS = DOWNSCALE_RATIO*4
MIN_PIXEL_VALUE = 0.1
MAX_PIXEL_VALUE = 5
GOAL_VALUE = 10
EDGE_VALUE = -10
REW_AFTER_VISIT = -0.5
reward = 0
RADIUS = 1 #radius around the end point that counts as reaching the goal

pos_ini = (VISIBLE_RADIUS+int(GRID_SIZE/2)+1,VISIBLE_RADIUS+int(GRID_SIZE/4))
